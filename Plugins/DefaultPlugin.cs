﻿using System.Globalization;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Orchard.ContentManagement;
using Orchard.DisplayManagement;

namespace Onestop.SlideShows.Plugins {
    public class DefaultPlugin : ISlideShowPlugin {
        public DefaultPlugin(IShapeFactory shapeFactory) {
            Shape = shapeFactory;
        }

        dynamic Shape { get; set; }

        public string Name { get { return "Default"; } }

        public int Order { get { return 100; } }

        public dynamic GetDisplayShape(ISlideShow slideShow) {
            return SetupConfigurationShape(Shape.DefaultSlideShowPlugin(), slideShow);
        }

        public dynamic GetConfigurationShape(ISlideShow slideShow) {
            return SetupConfigurationShape(Shape.DefaultSlideShowPlugin_Configuration(), slideShow);
        }

        public void UpdateConfiguration(string prefix, IUpdateModel updater, ISlideShow slideShow) {
            var updatedData = new UpdateViewModel();
            if (updater.TryUpdateModel(updatedData, prefix + ".DefaultPlugin", null, null)) {
                slideShow
                    .Set("AnimationSpeed", updatedData.AnimationSpeed.ToString(CultureInfo.InvariantCulture))
                    .Set("AnimationTime", updatedData.AnimationTime.ToString(CultureInfo.InvariantCulture))
                    .Set("Buttons", updatedData.Buttons.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Indicators", updatedData.Indicators.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Auto", updatedData.Auto.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("CycleOnce", updatedData.CycleOnce.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Cycling", updatedData.Cycling.ToString(CultureInfo.InvariantCulture).ToLowerInvariant())
                    .Set("Orientation", updatedData.Orientation.ToLowerInvariant())
                    .Set("MinWidth", updatedData.MinWidth.ToString(CultureInfo.InvariantCulture))
                    .Set("StopAtLast", updatedData.StopAtLast.ToString(CultureInfo.InvariantCulture).ToLowerInvariant());
            }
        }

        private dynamic SetupConfigurationShape(dynamic shape, ISlideShow slideShow) {
            shape.AnimationSpeed = slideShow.Get("AnimationSpeed", "1000");
            shape.AnimationTime = slideShow.Get("AnimationTime", "5000");
            shape.Buttons = slideShow.Get("Buttons", "true").ToLowerInvariant();
            shape.Indicators = slideShow.Get("Indicators", "false").ToLowerInvariant();
            shape.Auto = slideShow.Get("Auto", "true").ToLowerInvariant();
            shape.CycleOnce = slideShow.Get("CycleOnce", "false").ToLowerInvariant();
            shape.Cycling = slideShow.Get("Cycling", "true").ToLowerInvariant();
            shape.Orientation = slideShow.Get("Orientation", "horizontal").ToLowerInvariant();
            shape.MinWidth = slideShow.Get("MinWidth", "640");
            shape.StopAtLast = slideShow.Get("StopAtLast", "false").ToLowerInvariant();
            return shape;
        }

        private class UpdateViewModel {
            public int AnimationSpeed { get;  set; }
            public int AnimationTime { get;  set; }
            public bool Buttons { get;  set; }
            public bool Indicators { get;  set; }
            public bool Auto { get;  set; }
            public bool CycleOnce { get;  set; }
            public bool Cycling { get;  set; }
            public string Orientation { get;  set; }
            public int MinWidth { get;  set; }
            public bool StopAtLast { get;  set; }
        }
    }
}