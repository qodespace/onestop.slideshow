﻿using Onestop.SlideShows.Models;
using Orchard;
using Orchard.ContentManagement;

namespace Onestop.SlideShows.Services {
    public interface ISlideShowPlugin : IDependency {
        string Name { get; }
        int Order { get; }
        dynamic GetDisplayShape(ISlideShow slideShow);
        dynamic GetConfigurationShape(ISlideShow slideShow);
        void UpdateConfiguration(string prefix, IUpdateModel updater, ISlideShow slideShow);
    }
}