﻿using System.Collections.Generic;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Models;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.Core.Contents.ViewModels;
using Orchard.UI.Navigation;

namespace Onestop.SlideShows.Services {
    public interface ISlideShowService : IDependency {
        IEnumerable<SlideShowPart> GetSlideShows(out int count, Pager pager, ContentsOrder orderBy = ContentsOrder.Published);
        IEnumerable<SlidePart> GetSlides(IEnumerable<int> slideIds, VersionOptions versionOptions = null);
        IEnumerable<SlidePart> GetSlidesFor(SlideShowPart slideShow, VersionOptions versionOptions = null);
        IEnumerable<LayoutTemplatePart> GetLayoutsFor(IEnumerable<SlidePart> slides);
        IEnumerable<ISlideShowPlugin> GetPlugins();
        dynamic GetPluginDisplayShape(ISlideShow slideShow);
        IDictionary<string, dynamic> GetConfigurationEditorsFor(ISlideShow slideShow);
        void UpdatePluginConfiguration(ISlideShow slideShow, string prefix, IUpdateModel updater);
        void AddSlideTo(IContent container, string fieldName, SlidePart slide);
        void AddSlidesTo(IContent container, string fieldName, IEnumerable<SlidePart> slides);
        ISlideShow GetSlideShow(IContent container, string fieldName);
        IEnumerable<ContentTypeDefinition> GetSlideTypes();
    }
}
