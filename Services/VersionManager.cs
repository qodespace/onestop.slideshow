﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Records;
using Orchard.Data;

namespace Onestop.SlideShows.Services {
    public class VersionManager : IVersionManager {
        private readonly ISessionLocator _sessionLocator;
        private readonly IContentManager _contentManager;
        private readonly ITransactionManager _transactionManager;

        public VersionManager(ISessionLocator sessionLocator, IContentManager contentManager, ITransactionManager transactionManager) {
            _sessionLocator = sessionLocator;
            _contentManager = contentManager;
            _transactionManager = transactionManager;
        }

        public IEnumerable<ContentItem> GetLatest(IEnumerable<int> contentItemIds, QueryHints hints = null) {
            return GetLatest<ContentItem>(contentItemIds, hints);
        }

        public IEnumerable<T> GetLatest<T>(IEnumerable<int> contentItemIds, QueryHints hints = null) where T : class, IContent {
            var query = GetLatestVersionsQuery(contentItemIds);
            var items = LoadContentItems<T>(query, hints);

            return OrderByList<T>(items, contentItemIds);
        }

        private IEnumerable<T> LoadContentItems<T>(IQuery query, QueryHints hints = null) where T : class, IContent {
            var rows = query.List<object>();
            var versionIds = rows.Cast<object[]>().Select(x => (int)x[0]);
            return _contentManager.GetManyByVersionId<T>(versionIds, hints ?? QueryHints.Empty);
        }

        private IQuery GetLatestVersionsQuery(IEnumerable<int> contentItemIds = null) {
            var session = _transactionManager.GetSession(); 

            // Select only the highest versions.
            var select =
                "select max(ContentItemVersionRecord.Id), ContentItemVersionRecord.ContentItemRecord.Id, max(ContentItemVersionRecord.Number) " +
                "from Orchard.ContentManagement.Records.ContentItemVersionRecord ContentItemVersionRecord " +
                "join ContentItemVersionRecord.ContentItemRecord ContentItemRecord, Orchard.Core.Common.Models.CommonPartVersionRecord CommonPartVersionRecord " +
                "where CommonPartVersionRecord.Id = ContentItemVersionRecord.Id ";

            var filter = contentItemIds != null && contentItemIds.Any() ? "and ContentItemVersionRecord.ContentItemRecord.Id in (:ids) " : default(String);

            var group =
                "group by ContentItemVersionRecord.ContentItemRecord.Id";

            var hql = String.Concat(select, filter, group);
            var query = session.CreateQuery(hql);

            if (contentItemIds != null && contentItemIds.Any()) {
                query.SetParameterList("ids", contentItemIds.ToArray());
            }

            return query;
        }

        private static IEnumerable<T> OrderByList<T>(IEnumerable<T> items, IEnumerable<int> ids) where T : class, IContent {
            var dictionary = items.ToDictionary(x => x.Id);
            return ids.Select(id => dictionary[id]);
        }
    }
}