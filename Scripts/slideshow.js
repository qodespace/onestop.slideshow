﻿$(function() {
    var goToSlide = function(slideshow, pager, index, direction, transition) {
        var oldIndex = slideshow.data("current-slide"),
            numberOfSlides = +(slideshow.data("number-of-slides")
                || slideshow.find(".templated-item").length),
            speed = slideshow.data("speed") || "slow";

        index = index % numberOfSlides;
        if (index < 0) index += numberOfSlides;
        if (oldIndex == index) return;
        slideshow.data({
            currentSlide: index,
            numberOfSlides: numberOfSlides
        });
        pager.find(".slide-number").removeClass("slide-current");
        pager.find(".slide-number-" + index).addClass("slide-current");
        if (!transition) transition = slideshow.data("transition");
        switch ((transition || "").toLowerCase()) {
            case "fade":
                var slides = slideshow.find(".templated-item"),
                    oldSlide = $(slides[oldIndex]),
                    newSlide = $(slides[index]);
                oldSlide.fadeOut(speed, function() { oldSlide.parent().hide(); });
                newSlide.parent().show();
                newSlide.fadeIn(speed);
                break;
            case "none":
                var list = slideshow.find("ul").first(),
                    newLiIndex = index + 1,
                    finalIndex = index + 1;
                ensureClonedFirstAndLast(list);
                if (oldIndex == numberOfSlides - 1 && index == 0 && direction !== "previous") {
                    newLiIndex = numberOfSlides + 1;
                    finalIndex = 1;
                } else if (oldIndex == 0 && index == numberOfSlides - 1 && direction !== "next") {
                    newLiIndex = 0;
                    finalIndex = numberOfSlides;
                }
                list.css("left", -finalIndex + "00%");
                break;
            default:
                var list = slideshow.find("ul").first(),
                    newLiIndex = index + 1,
                    finalIndex = index + 1;
                ensureClonedFirstAndLast(list);
                if (oldIndex == numberOfSlides - 1 && index == 0 && direction !== "previous") {
                    newLiIndex = numberOfSlides + 1;
                    finalIndex = 1;
                } else if (oldIndex == 0 && index == numberOfSlides - 1 && direction !== "next") {
                    newLiIndex = 0;
                    finalIndex = numberOfSlides;
                }
                list.animate({ left: -newLiIndex + "00%" }, speed, function() {
                    list.css("left", -finalIndex + "00%");
                });
                break;
        }
    },
        ensureClonedFirstAndLast = function(list) {
            if (!list.data("cloned")) {
                var first = list.children().first(),
                    last = list.children().last();
                list.prepend(last.clone())
                    .append(first.clone())
                    .css("left", "-100%")
                    .data("cloned", true);
            }
        };

    $(".slide-show-pager a,.slide-show a.arrow,.slide-show .pager a, div.next a").click(function(event) {
        event.preventDefault();
        var slideShowId = $(this).data('slide-show-id');
        var pageButton = $(this).closest(".slide-pager-button,.arrow,.next a"),
            pager = $(".slide-show-pager, .slide-show-pager[data-slide-show-id='" + slideShowId + "'], .pager"),
            slideshow = $("#" + (pageButton.data("slide-show-id") || pager.data("slide-show-id"))),
            currentSlide = slideshow.data("current-slide");
        if (pager.length == 0) pager = slideshow.closest(".slide-show").find(".slide-show-pager,.pager");
        if (pageButton.is(".slide-previous,.previous")) {
            goToSlide(slideshow, pager, currentSlide - 1, "previous");
        } else if (pageButton.is(".slide-next,.next,.next a")) {
            goToSlide(slideshow, pager, currentSlide + 1, "next");
        } else if (pageButton.hasClass("slide-number")) {
            goToSlide(slideshow, pager, pageButton.data("index"));
        }
        return false;
    });

    $(function() {
        // Have the slideshows that are rendered with the data-slide-index attribute jump to the configured slide index
        $(".slide-show[data-slide-index]").each(function() {
            var slideShowId = $(this).data('slide-show-id');
            var slideIndex = parseInt($(this).data('slide-index'));
            var slideshow = $("#" + slideShowId);
            var pager = $(this).find(".slide-show-pager, .slide-show-pager[data-slide-show-id='" + slideShowId + "'], .pager");

            goToSlide(slideshow, pager, slideIndex, null, "none");
        });
    });
});
