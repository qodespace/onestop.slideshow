/* ONESTOP JAVASCRIPT MODULAR FRAMEWORK */
/* CORE */
/* https://confluence.onestop.com/display/ENG/Framework */

var OS = OS || { client: "ONESTOP", path: "/Modules/Onestop.SlideShows/Scripts/", env: "live" };
OS = (function () {
    // constants
    VERSION = "1.0";
    LOCAL = "local";
    DEV = "dev";
    QA = "qa";
    STAGE = "stage";
    LIVE = "live";
    TEST = "test";
    ERROR_PAGE = "/error_404.html";
    JQUERY_1_9_1 = "jquery-1.9.1.min.js";
    JQUERY_1_8_0 = "jquery.min.1.8.0.js";
    JQUERY_1_7_2 = "jquery-1.7.2.min.js";
    KNOCKOUT_2_2_1 = "knockout/knockout-2.2.1.min.js";
    ELEVATE_ZOOM_3_0_3 = "jquery.elevatezoom-3.0.3.min.js";
    OSPATH = "/store/os/js/";
    var dependencyCount = 0;
    var dependencyLoaded = 0;
    var dependencyArray = [];
    var callbackArray = [];
    var cbt;
    // methods
    return {
        // required methods
        init: function (currentClient, newPath, newEnv) {
            OS.config(currentClient, newPath, newEnv);
            OS.log("** namespace initialized **");
            OS.pages();
            OS.utilities();
            OS.modules();

        },
        config: function (newClient, newPath, newEnv) {
            OS.setClient(newClient);
            OS.setPath(newPath);
            OS.setEnv(newEnv);
        },

        // getters
        getVersion: function () {
            return VERSION;
        },
        getClient: function () {
            return (OS.isValidObject(client)) ? client : "client is not yet defined";
        },
        getPath: function () {
            return (OS.isValidObject(path)) ? path : OSPATH;
        },
        getEnv: function () {
            return (OS.isValidObject(env)) ? env : "environment is not yet defined";
        },
        setDependencyCount: function (count) {
            dependencyCount = count;
        },

        // setters
        setClient: function (val) {
            client = (OS.isValidObject(val)) ? val : "ONESTOP";
        },
        setPath: function (val) {
            path = (OS.isValidObject(val)) ? val : OSPATH;
        },
        setEnv: function (val) {
            if (OS.isValidObject(val)) {
                env = val;
            } else if (OS.isValidObject(client)) {
                if (OS.contains(document.domain, ".local.")) {
                    env = LOCAL;
                } else if (OS.contains(document.domain, ".dev.")) {
                    env = DEV;
                } else if (OS.contains(document.domain, ".qa.")) {
                    env = QA;
                } else if (OS.contains(document.domain, ".stage.")) {
                    env = STAGE;
                } else {
                    env = LIVE;
                }
            } else {
                env = LIVE;
            }
        },
        // initialization
        utilities: function () {
            var u;
            try {
                for (u in OS.util) {
                    if (typeof (OS.util[u].initialized) === "undefined") {
                        OS.util[u].init();
                        OS.util[u].initialized = true;
                    }
                }
            }
            catch (e) { OS.error("Error initializing utilities: " + e.message); }
        },
        pages: function () {
            try {
                for (var p in OS.page) {
                    OS.page[p].init();
                }
            }
            catch (e) { OS.error("Error initializing page: " + e.message); }
        },
        modules: function () {
            try {
                for (var m in OS.mod) {
                    if (typeof (OS.mod[m].initialized) == "undefined") {
                        OS.mod[m].init();
                        OS.mod[m].initialized = true;
                    }
                }
            }
            catch (e) { OS.error("Error initializing modules: " + e.message); }
        },
        load: function (dependencies, callback) {
            for (var d = 0; d < dependencies.length; d++) {
                if (OS.inArray(dependencies[d], dependencyArray) === false) {
                    dependencyArray.push(dependencies[d]);
                }
            }
            OS.setDependencyCount(dependencyArray.length);
            try {
                for (i = 0; i < dependencies.length; i++) {
                    var isLast = (i == (dependencies.length - 1)) ? true : false;
                    if (OS.contains(dependencies[i], "jquery") && !OS.contains(dependencies[i], "elevatezoom")) {
                        OS.loadLibrary(window.jQuery, dependencies[i], callback, isLast);
                    } else if (OS.contains(dependencies[i], "knockout")) {
                        OS.loadLibrary(window.ko, dependencies[i], callback, isLast);
                    } else if (OS.contains(dependencies[i], "elevatezoom")) {
                        OS.loadPlugin("OS.mod.elevateZoom", dependencies[i], callback, isLast);
                    } else if (!OS.isValidObject(eval(dependencies[i]))) {
                        OS.loadScript(dependencies[i].toLowerCase(), dependencies[i], callback, isLast);
                    } else if (OS.isValidObject(eval(dependencies[i])) && isLast) {
                        OS.modules();
                        OS.utilities();
                        OS.callbackHelper(callback);
                    }
                }
            }
            catch (e) {
                OS.error("Error loading dependencies: " + e.message);
            }
        },
        loadPlugin: function (obj, dependency, callback, isLast) {
            if (OS.isValidObject(dependency)) {
                var fileref = document.createElement('script');
                fileref.type = 'text/javascript';
                if (OS.isValidObject(callback)) {
                    fileref.onreadystatechange = function () {
                        if (this.readyState == 'loaded' || this.readyState == 'complete') {
                            OS.loadCallback(dependency, callback, isLast);
                        }
                    };
                    fileref.onload = OS.loadCallback(obj, callback, isLast);
                }
                fileref.src = path + dependency;
                document.getElementsByTagName("body")[0].appendChild(fileref);
            }
        },
        loadLibrary: function (obj, dependency, callback, isLast) {
            if (!OS.isValidObject(obj) && OS.isValidObject(dependency)) {
                var fileref = document.createElement('script');
                fileref.type = 'text/javascript';
                if (OS.isValidObject(callback)) {
                    fileref.onreadystatechange = function () {
                        if (this.readyState == 'loaded' || this.readyState == 'complete') {
                            OS.loadCallback(dependency, callback, isLast);
                        }
                    };
                    fileref.onload = OS.loadCallback(obj, callback, isLast);
                }
                fileref.src = path + dependency;
                document.getElementsByTagName("body")[0].appendChild(fileref);
            }
        },
        loadScript: function (url, dependency, callback, isLast) {
            if (OS.isValidObject(url)) {
                var fileref = document.createElement('script');
                fileref.type = 'text/javascript';
                if (OS.isValidObject(callback)) {
                    fileref.onreadystatechange = function () {
                        if (this.readyState == 'loaded' || this.readyState == 'complete') {
                            OS.loadCallback(dependency, callback, isLast);
                        }
                    };
                    fileref.onload = OS.loadCallback(dependency, callback, isLast);
                }
                fileref.src = OS.getUrl(url);
                document.getElementsByTagName("body")[0].appendChild(fileref);
            }
        },
        loadCallback: function (model, callback, isLast) {
            if (OS.inArray(callback, callbackArray) === false) {
                callbackArray.push(callback);
            }
            if (OS.isValidObject(eval(model)) || typeof (elevateZoom) !== "undefined") {
                dependencyLoaded = dependencyLoaded + 1;
                OS.log("dependencyLoaded: " + dependencyLoaded + " = " + dependencyCount);
                if (dependencyLoaded == dependencyCount) {
                    clearTimeout(cbt);
                    for (var i = 0; i < callbackArray.length; i++) {
                        OS.callbackHelper(callbackArray[i]);
                    }
                }
            }
            else {
                try {
                    cbt = setTimeout(function () {
                        if (dependencyLoaded < dependencyCount) {
                            OS.loadCallback(model, callback, isLast);
                        }
                    }, 10);
                }
                catch (e) {
                    OS.error("Error finalizing loading dependencies: " + e.message);
                }
            }
        },
        log: function () {
            if (!OS.isEnv(LIVE) && OS.isValidObject(window.console)) {
                window.console.log(Array.prototype.join.call(arguments, ''));
            }
        },
        error: function () {
            if (OS.isEnv(LIVE)) {
                // TODO: implement error log handling for production environment
                /* TEMP */OS.log(Array.prototype.join.call(arguments, ''));
            } else {
                OS.log(Array.prototype.join.call(arguments, ''));
            }
        },
        fatal: function () {
            OS.error(Array.prototype.join.call(arguments, ''));
            if (!OS.isEnv(TEST)) {
                OS.redirect(ERROR_PAGE);
            }
        },

        // handlers
        clickHandler: function (sel, callback, args) {
            if (OS.isValidObject(sel) && document.getElementById(sel) !== null) {
                document.getElementById(sel).onclick = function () {
                    OS.callbackHelper(callback, args);
                };
            }
        },
        clickHandlerByClass: function (sel, callback, args) {
            if (OS.isValidObject(sel)) {
                var tabs = OS.getElementsByClass(document.body, sel);
                if (OS.isValidObject(tabs)) {
                    var elsArray = Array.prototype.slice.call(tabs, 0);
                    OS.foreach();
                    elsArray.forEach(function (el) {
                        el.onclick = function () {
                            OS.callbackHelper(callback, args);
                        };
                    });
                }
            }
        },
        onChangeHandler: function (sel, callback, args) {
            if (OS.isValidObject(sel) && document.getElementById(sel) !== null) {
                document.getElementById(sel).onchange = function () {
                    OS.callbackHelper(callback, args);
                };
            }
        },
        onChangeHandlerByClass: function (sel, callback, args) {
            if (OS.isValidObject(sel)) {
                var tabs = OS.getElementsByClass(document.body, sel);
                if (OS.isValidObject(tabs)) {
                    var elsArray = Array.prototype.slice.call(tabs, 0);
                    OS.foreach();
                    elsArray.forEach(function (el) {
                        el.onchange = function () {
                            OS.callbackHelper(callback, args);
                        };
                    });
                }
            }
        },
        resizeHandler: function (callback, args) {
            window.onresize = function () {
                OS.callbackHelper(callback, args);
            };
        },

        // helpers
        isClient: function (clientName) {
            return (OS.getClient() === clientName);
        },
        isEnv: function (envName) {
            return (OS.getEnv() === envName);
        },
        isValidObject: function (obj) {
            return (typeof obj != 'undefined' && obj !== null && obj !== "" && obj.length !== 0);
        },
        contains: function (str, cnt) {
            return str.indexOf(cnt) !== -1;
        },
        redirect: function (url) {
            if (OS.isValidObject(url)) { window.location = url; }
            else { OS.error("no url supplied"); }
        },
        inArray: function (str, arr) {
            for (var i = 0; i < arr.length; i++) {
                if (str === arr[i]) {
                    return true;
                }
            }
            return false;
        },
        foreach: function () {
            if (!('forEach' in Array.prototype)) {
                Array.prototype.forEach = function (action, that /*opt*/) {
                    for (var i = 0, n = this.length; i < n; i++) {
                        if (i in this) {
                            action.call(that, this[i], i, this);
                        }
                    }
                };
            }
        },
        getElementsByClass: function (node, classname) {
            if (!document.getElementsByClassName) {
                var a = [];
                var re = new RegExp('(^| )' + classname + '( |$)');
                var els = node.getElementsByTagName("*");
                for (var i = 0, j = els.length; i < j; i++) {
                    if (re.test(els[i].className)) {
                        a.push(els[i]);
                    }
                }
                return a;
            }
            return document.getElementsByClassName(classname);
        },
        callbackHelper: function (callback, args) {
            try {
                if (OS.isValidObject(args)) {
                    eval(callback)(args);
                } else {
                    eval(callback)();
                }
            }
            catch (e) {
                OS.error("Callback failed: " + e.message + " - " + callback);
            }
        },
        getUrl: function (mod) {
            return (OS.isEnv(LOCAL) || OS.isEnv(DEV)) ? path + "framework/" + VERSION + "/" + mod + ".js" : path + "framework/" + VERSION + "/" + mod + ".min.js";
        }
    };
} ());
OS.util = OS.util || {};
OS.mod = OS.mod || {};
OS.page = OS.page || {};
/*** ONESTOP JAVASCRIPT MODULAR FRAMEWORK ***/