﻿using System.Collections.Generic;
using Onestop.Layouts.Models;
using Onestop.Layouts.Settings;

namespace Onestop.SlideShows.ViewModels {
    public class SlideViewModel {
        public int SlideShowId { get; set; }
        public string FieldName { get; set; }
        public IEnumerable<string> MediaFolders { get; set; } 
        public string ReturnUrl { get; set; }
        public TemplatedItemPart TemplatedItem { get; set; }
        public LayoutTemplatePart Template { get; set; }
        public IEnumerable<LayoutTemplatePart> Templates { get; set; }
        public IEnumerable<dynamic> EditorShapes { get; set; }
        public dynamic TemplatedItemPreviewShape { get; set; }
        public IDictionary<string, string> SlideContentTypes { get; set; }
        public string DefaultSlideType { get; set; }
        public IDictionary<string, TemplatedItemPartSettings> TemplatedItemSettings { get; set; } 
    }
}
