﻿Name: Onestop Slide Shows
AntiForgery: enabled
Author: Bertrand Le Roy / Onestop
Website: https://bitbucket.org/onestop/onestop.slideshow/branch/feature%2FOSPD-3005
Version: 1.9.1
OrchardVersion: 1.9.1
Description: Creates rich slide shows with multiple positioned photos and text boxes
Features:
    Onestop.SlideShows
        Name: Onestop Slide Shows
        Description: Creates rich slide shows with multiple positioned photos and text boxes
        Category: Content
        Dependencies: Orchard.Projections, Orchard.MediaLibrary, Orchard.Forms, Onestop.Layouts, Orchard.Tokens 
	Onestop.SlideShows.Draftable
        Name: Onestop Slide Shows - Publish Later -- ALPHA DO NOT USE
        Description: Adds the ability to schedule individual slides and slideshows
		Category: Content
        Dependencies: Onestop.SlideShows, Orchard.ArchiveLater, Orchard.PublishLater