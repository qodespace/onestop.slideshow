﻿using System;
using System.Collections.Generic;
//
using Onestop.SlideShows.Models;
using Orchard.ContentManagement.MetaData.Models;

namespace Onestop.SlideShows.Settings {
    public class SlideShowSettings : ISlideShow {
        public string Hint { get; set; }
        public IEnumerable<int> SlideIds { get; set; }
        public IEnumerable<SlidePart> Slides { get; 
            //
            private set; }
        public string Plugin { get; set; }
        public string SettingsPrefix { get; set; }
        public SettingsDictionary Settings { get; set; }
        public Action<string, string> SettingSetter { get; set; }
        public string Get(string settingName, string defaultValue = null) {
            if (Settings == null) return defaultValue;
            var settings = Settings.GetModel<Dictionary<string, string>>(
                SettingsPrefix ?? "SlideShowSettings");
            string setting;
            if (settings.TryGetValue(settingName, out setting)) {
                return setting ?? defaultValue;
            }
            return defaultValue;
        }

        public ISlideShow Set(string settingName, string value) {
            if (SettingSetter != null) {
                SettingSetter(settingName, value);
                return this;
            }
            var settings = Settings.GetModel<Dictionary<string, string>>(
                SettingsPrefix ?? "SlideShowSettings");
            settings[settingName] = value;
            return this;
        }

        public IDictionary<string, dynamic> PluginConfigurationEditors { get; set; }
        public bool AllowPluginOverride { get; set; }
        public bool AllowPluginConfigurationOverride { get; set; }
        public bool PreventTemplateChoice { get; set; }
        public bool AllowSlideTypeChoice { get; set; }
        public string DefaultSlideType { get; set; }
        public IDictionary<string, string> SlideContentTypes { get; set; } 
    }
}
