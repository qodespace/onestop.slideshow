﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onestop.SlideShows.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.MetaData.Builders;
using Orchard.ContentManagement.MetaData.Models;
using Orchard.ContentManagement.ViewModels;

namespace Onestop.SlideShows.Settings {
    public class SlideShowEditorEvents : ContentDefinitionEditorEventsBase {
        private readonly ISlideShowService _slideShowService;

        public string Prefix { get { return "SlideShowSettings"; } }

        public SlideShowEditorEvents(ISlideShowService slideShowService) {
            _slideShowService = slideShowService;
        }

        public override IEnumerable<TemplateViewModel> PartFieldEditor(ContentPartFieldDefinition definition) {
            if (definition.FieldDefinition.Name == "SlideShowField") {
                var model = BuildSlideShowSettings(definition.Settings);
                yield return DefinitionTemplate(model);
            }
        }

        public override IEnumerable<TemplateViewModel> TypePartEditor(ContentTypePartDefinition definition) {
            if (definition.PartDefinition.Name == "SlideShowPart") {
                var model = BuildSlideShowSettings(definition.Settings);
                yield return DefinitionTemplate(model);
            }
        }

        private SlideShowSettings BuildSlideShowSettings(SettingsDictionary settings) {
            var model = settings.GetModel<SlideShowSettings>();
            model.Settings = settings;
            model.PluginConfigurationEditors = _slideShowService.GetConfigurationEditorsFor(model);
            model.SlideContentTypes = _slideShowService.GetSlideTypes().ToDictionary(d => d.Name, d => d.DisplayName);
            model.DefaultSlideType = String.IsNullOrWhiteSpace(model.DefaultSlideType)
                ? "Slide"
                : model.DefaultSlideType;

            if (String.IsNullOrWhiteSpace(model.Plugin))
                model.Plugin = "Default";

            return model;
        }

        public override IEnumerable<TemplateViewModel> PartFieldEditorUpdate(ContentPartFieldDefinitionBuilder builder, IUpdateModel updateModel) {
            if (builder.FieldType != "SlideShowField") {
                yield break;
            }

            var model = SlideShowSettings(builder, updateModel);

            yield return DefinitionTemplate(model);
        }

        public override IEnumerable<TemplateViewModel> TypePartEditorUpdate(ContentTypePartDefinitionBuilder builder,
                                                                            IUpdateModel updateModel) {
            if (builder.Name != "SlideShowPart") {
                yield break;
            }

            var model = SlideShowSettings(builder, updateModel);

            yield return DefinitionTemplate(model);
        }

        private SlideShowSettings SlideShowSettings(object builder, IUpdateModel updateModel)
        {
            var model = new SlideShowSettings();
            model.SlideContentTypes = _slideShowService.GetSlideTypes().ToDictionary(d => d.Name, d => d.DisplayName);
            Action<string, string> withSetting = model.SettingSetter =
                builder is ContentPartFieldDefinitionBuilder
                    ? (name, value) => ((ContentPartFieldDefinitionBuilder) builder).WithSetting(Prefix + "." + name, value)
                    : new Action<string, string>((name, value) => ((ContentTypePartDefinitionBuilder)builder).WithSetting(Prefix + "." + name, value));

            if (updateModel.TryUpdateModel(model, Prefix, null, null)) {
                withSetting("Hint", model.Hint);
                withSetting("AllowPluginOverride",
                    model.AllowPluginOverride.ToString());
                withSetting("AllowPluginConfigurationOverride",
                    model.AllowPluginConfigurationOverride.ToString());
                withSetting("PreventTemplateChoice",
                    model.PreventTemplateChoice.ToString());
                withSetting("AllowSlideTypeChoice",
                    model.AllowSlideTypeChoice.ToString());
                withSetting("DefaultSlideType", model.DefaultSlideType);

                withSetting("Plugin", model.Plugin);
                _slideShowService.UpdatePluginConfiguration(model, Prefix, updateModel);
            }
            model.PluginConfigurationEditors = _slideShowService.GetConfigurationEditorsFor(model);
            return model;
        }
    }
}