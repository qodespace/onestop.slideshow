﻿using System.IO;
using System.Linq;
using System.Web;
using Orchard.ContentManagement;
using Orchard.ContentPicker.Fields;
using Orchard.DisplayManagement;
using Orchard.DisplayManagement.Descriptors;
using Orchard.DisplayManagement.Shapes;
using Orchard.Environment;

namespace Onestop.SlideShows.Shapes {
    public class SlideShowWidgetShapeAlterations : IShapeTableProvider {
        private readonly Work<IContentManager> _contentManager;

        public SlideShowWidgetShapeAlterations(Work<IContentManager> contentManager) {
            _contentManager = contentManager;
        }

        public void Discover(ShapeTableBuilder builder) {
            builder
                .Describe("Fields_ContentPicker")
                .OnDisplaying(context => {
                    var field = (ContentPickerField) context.Shape.ContentField;
                    var item = (ContentItem) context.Shape.ContentItem;
                    var metadata = (ShapeMetadata) context.Shape.Metadata;
                    if (metadata.DisplayType == "Detail" &&
                        field.Name == "SlideShow" &&
                        item != null &&
                        item.ContentType == "SlideShowWidget") {

                        metadata.Type = "SlideShowWidget_SlideShow";
                    }
                });
        }

        [Shape]
        public void SlideShowWidget_SlideShow(
            dynamic Display,
            TextWriter Output,
            ContentPickerField ContentField)
        {

            var contentItems = ContentField.ContentItems.ToList();
            if (contentItems.Any()) {
                var slideShow = contentItems.First();
                var slideShowShape = _contentManager.Value.BuildDisplay(slideShow, "Detail");
                Output.Write(Display(slideShowShape.Content));
            }
        }
    }
}