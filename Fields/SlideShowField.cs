﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onestop.SlideShows.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.FieldStorage;
using Orchard.ContentManagement.Utilities;

namespace Onestop.SlideShows.Fields {
    public class SlideShowField : ContentField, ISlideShow {
        private IDictionary<string, string> _settings;
        private IDictionary<string, string> _defaultConfiguration;

        internal readonly LazyField<IEnumerable<SlidePart>> LazySlides =
            new LazyField<IEnumerable<SlidePart>>();

        public IEnumerable<int> SlideIds {
            get {
                var ids = Storage.Get<string>("slides");
                return DecodeIds(String.IsNullOrWhiteSpace(ids) ? Storage.Get<string>() : ids);
            }
            set { Storage.Set("slides", EncodeIds(value)); }
        }

        public IEnumerable<SlidePart> Slides {
            get { return LazySlides.Value; }
        }

        public string Plugin {
            get { return Storage.Get<string>("plugin"); }
            set { Storage.Set("plugin", value); }
        }

        public bool AllowSlideTypeChoice {
            get { return Storage.Get<bool>("AllowSlideTypeChoice"); }
            set { Storage.Set("AllowSlideTypeChoice", value); }
        }

        public string DefaultSlideType {
            get { return Storage.Get<string>("DefaultSlideType"); }
            set { Storage.Set("DefaultSlideType", value); }
        }

        public string Get(string settingName, string defaultValue = null) {
            string value;
            if (_settings != null) {
                return _settings.TryGetValue(settingName, out value) ? value ?? defaultValue : defaultValue;
            }
            value = Storage.Get<string>(settingName);
            if (string.IsNullOrWhiteSpace(value)
                    && _defaultConfiguration != null
                    && _defaultConfiguration.TryGetValue(settingName, out value)) {
                        return value ?? defaultValue;
            }
            return value ?? defaultValue;
        }

        public ISlideShow Set(string settingName, string value) {
            Storage.Set(settingName, value);
            return this;
        }

        private static string EncodeIds(IEnumerable<int> ids) {
            var list = ids.ToList();
            if (ids == null || !list.Any()) {
                return string.Empty;
            }

            return string.Join(",", list);
        }

        private static IEnumerable<int> DecodeIds(string ids) {
            if (String.IsNullOrWhiteSpace(ids)) {
                return Enumerable.Empty<int>();
            }

            return ids
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(Int32.Parse);
        }

        public void SetConfiguration(IDictionary<string, string> settings) {
            _settings = settings;
        }

        public void SetFallbackConfiguration(Dictionary<string, string> defaultConfig) {
            _defaultConfiguration = defaultConfig;
        }
    }
}
