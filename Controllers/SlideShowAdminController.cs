﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using NHibernate.Linq;
using Onestop.Layouts.Models;
using Onestop.Layouts.Services;
using Onestop.Layouts.Settings;
using Onestop.SlideShows.Fields;
using Onestop.SlideShows.Services;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Settings;
using Onestop.SlideShows.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.Core.Contents.ViewModels;
using Orchard.DisplayManagement;
using Orchard.Environment.Extensions;
using Orchard.Localization;
using Orchard.MediaLibrary.Models;
using Orchard.Mvc.Html;
using Orchard.Settings;
using Orchard.UI.Admin;
using Orchard.UI.Navigation;
using Orchard.UI.Resources;

namespace Onestop.SlideShows.Controllers {
    [OrchardFeature("Onestop.SlideShows")]
    [ValidateInput(false)]
    [Admin]
    public class SlideShowAdminController : Controller {
        private readonly ISlideShowService _slideShowService;
        private readonly ITemplateService _templateService;
        private readonly IContentManager _contentManager;
        private readonly ISiteService _siteService;
        private readonly IResourceManager _resourceManager;

        public SlideShowAdminController(
            ISlideShowService slideShowService,
            ITemplateService templateService,
            IOrchardServices services,
            IContentManager contentManager,
            ISiteService siteService,
            IResourceManager resourceManager,
            IShapeFactory shapeFactory) {

            _slideShowService = slideShowService;
            _templateService = templateService;
            _contentManager = contentManager;
            _siteService = siteService;
            _resourceManager = resourceManager;
            Services = services;
            T = NullLocalizer.Instance;
            Shape = shapeFactory;
        }

        private dynamic Shape { get; set; }
        public Localizer T { get; set; }
        public IOrchardServices Services { get; set; }

        public ActionResult List(ListContentsViewModel model, PagerParameters pagerParameters) {
            if (!Services.Authorizer.Authorize(Permissions.ManageSlideShows, T("Not allowed to manage slide shows.")))
                return new HttpUnauthorizedResult();

            var pager = new Pager(_siteService.GetSiteSettings(), pagerParameters);
            int count;
            var pageOfContentItems = _slideShowService.GetSlideShows(out count, pager, model.Options.OrderBy);
            var pagerShape = Shape.Pager(pager).TotalItemCount(count);

            var list = Shape.List();
            list.AddRange(pageOfContentItems.Select(ci => _contentManager.BuildDisplay(ci, "SummaryAdmin")));

            dynamic viewModel = Shape.ViewModel(
                ContentItems: list,
                Pager: pagerShape,
                Options: model.Options);

            // Casting to avoid invalid (under medium trust) reflection over the protected View method and force a static invocation.
            return View((object) viewModel);
        }

        public ActionResult CreateSlide(int slideShowId, string fieldName, string returnUrl) {
            if (!Services.Authorizer.Authorize(Permissions.ManageSlideShows, T("Not allowed to manage slide shows."))) {
                return new HttpUnauthorizedResult();
            }

            var layouts = _templateService.GetTemplates();
            var model = new SlideViewModel {
                Templates = layouts,
                SlideShowId = slideShowId,
                FieldName = fieldName,
                ReturnUrl = returnUrl
            };

            SlideShowSettings slideShowSettings;
            var allowSlideTypeChoice = false;
            var defaultSlideType = "Slide";

            var slideShowItem = _contentManager.Get(slideShowId, VersionOptions.Latest);
            if (slideShowItem.Has<SlideShowPart>()
                && String.IsNullOrWhiteSpace(fieldName)) {

                var slideShow = slideShowItem.As<SlideShowPart>();
                slideShowSettings = slideShow.TypePartDefinition.Settings.GetModel<SlideShowSettings>();
                allowSlideTypeChoice = slideShowSettings.AllowSlideTypeChoice
                                       && slideShow.AllowSlideTypeChoice;
                defaultSlideType = String.IsNullOrWhiteSpace(slideShow.DefaultSlideType)
                    ? String.IsNullOrWhiteSpace(slideShowSettings.DefaultSlideType)
                        ? "Slide"
                        : slideShowSettings.DefaultSlideType
                    : slideShow.DefaultSlideType;
            }
            else {
                var slideShowField = GetSlideShowField(fieldName, slideShowItem);
                if (slideShowField != null) {
                    slideShowSettings = slideShowField.PartFieldDefinition.Settings.GetModel<SlideShowSettings>();
                    allowSlideTypeChoice = slideShowSettings.AllowSlideTypeChoice
                                           && slideShowField.AllowSlideTypeChoice;
                    defaultSlideType = String.IsNullOrWhiteSpace(slideShowField.DefaultSlideType)
                        ? String.IsNullOrWhiteSpace(slideShowSettings.DefaultSlideType)
                            ? "Slide"
                            : slideShowSettings.DefaultSlideType
                        : slideShowField.DefaultSlideType;
                }
            }
            
            var slideTypes = _slideShowService.GetSlideTypes();
            model.TemplatedItemSettings =
                slideTypes
                    .Where(t => t.Parts.Any(p => p.PartDefinition.Name == "TemplatedItemPart"))
                    .ToDictionary(t => t.Name, t => t.Parts.First(p => p.PartDefinition.Name == "TemplatedItemPart").Settings.GetModel<TemplatedItemPartSettings>());

            if (allowSlideTypeChoice) {
                model.SlideContentTypes = slideTypes.ToDictionary(t => t.Name, t => t.DisplayName);
            }
            model.DefaultSlideType = defaultSlideType;

            _resourceManager.Require("script", "jQuery");

            return View(model);
        }

        [HttpPost]
        [Orchard.Mvc.FormValueRequired("submit.Save")]
        public ActionResult CreateSlide(
            int slideShowId, 
            string fieldName, 
            string slideType, 
            int templateId, 
            string returnUrl) {

            if (!Services.Authorizer.Authorize(Permissions.ManageSlideShows, T("Not allowed to manage slide shows.")))
                return new HttpUnauthorizedResult();

            var slideContainer = _contentManager.Get(slideShowId, VersionOptions.DraftRequired);
            if (slideContainer == null)
                throw new ArgumentNullException("slideShowId", "New slides must have an existing container.");

            var layout = _contentManager.Get<LayoutTemplatePart>(templateId);
            var slide = _contentManager.New<SlidePart>(slideType);

            _contentManager.Create(slide);
            _slideShowService.AddSlideTo(slideContainer, fieldName, slide);
            slide.As<TemplatedItemPart>().LayoutId = layout.Id;

            var metadata = _contentManager.GetItemMetadata(slide);
            if (metadata.AdminRouteValues != null && !String.IsNullOrWhiteSpace(returnUrl)) {
                metadata.AdminRouteValues.Add("returnUrl", returnUrl);
                return new RedirectToRouteResult(metadata.AdminRouteValues);
            }
            return new RedirectResult(Url.ItemEditUrl(slide, new {returnUrl}));
        }

        public ActionResult AddExistingMediaSlide(int slideShowId, string fieldName, string returnUrl) {
            if (!Services.Authorizer.Authorize(Permissions.ManageSlideShows, T("Not allowed to manage slide shows.")))
                return new HttpUnauthorizedResult();

            var slide = _contentManager.New<SlidePart>("Slide");
            var model = new SlideViewModel {
                TemplatedItem = slide.As<TemplatedItemPart>(),
                SlideShowId = slideShowId,
                FieldName = fieldName,
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [HttpPost]
        [Orchard.Mvc.FormValueRequired("submit.Save")]
        public ActionResult AddExistingMediaSlide(int slideShowId, string fieldName, string selectedIds, string returnUrl) {
            if (!Services.Authorizer.Authorize(Permissions.ManageSlideShows, T("Not allowed to manage slide shows.")))
                return new HttpUnauthorizedResult();

            var slideContainer = _contentManager.Get(slideShowId, VersionOptions.DraftRequired);
            if (slideContainer == null)
                throw new ArgumentNullException("slideShowId", "New slides must have an existing container.");

            var mediaIds = selectedIds
                .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
                .Select(id => Convert.ToInt32(id));
            var newSlides = _contentManager.GetMany<MediaPart>(mediaIds, VersionOptions.Latest, QueryHints.Empty)
                .Select(m => {
                    var mediaType = m.LogicalType;
                    if (mediaType.Equals("image", StringComparison.InvariantCultureIgnoreCase)) {
                        var slide = _contentManager.New<SlidePart>("Slide");
                        _contentManager.Create(slide);
                        AddMediaToSlide(slide, "img", m.MediaUrl);
                        return slide;
                    }
                    if (mediaType.Equals("OEmbed", StringComparison.InvariantCultureIgnoreCase)) {
                        var slide = _contentManager.New<SlidePart>("Slide");
                        _contentManager.Create(slide);
                        AddMediaToSlide(slide, "video", m.As<OEmbedPart>().Source);
                        return slide;
                    }
                    return null;
                })
                .Where(m => m != null)
                .ToList();
            AddNewSlidesToSlideShow(fieldName, slideContainer, newSlides);
            return new RedirectResult(returnUrl);
        }

        private void AddNewSlidesToSlideShow(string fieldName, ContentItem slideContainer, IEnumerable<SlidePart> newSlides) {
            _slideShowService.AddSlidesTo(slideContainer, fieldName, newSlides);
        }
 
        private static void AddMediaToSlide(SlidePart slide, string mediaType, string mediaPublicUrl) {
            var templatedPart = slide.As<TemplatedItemPart>();
            var img = new XElement(
                mediaType,
                new XAttribute("src", mediaPublicUrl));
            templatedPart.Data = new XElement("data", img).ToString(SaveOptions.DisableFormatting);
        }

        private static SlideShowField GetSlideShowField(string fieldName, ContentItem slideContainer) {
            return slideContainer
                .Parts
                .SelectMany(p => p.Fields)
                .FirstOrDefault(f => f.Name.Equals(fieldName, StringComparison.InvariantCultureIgnoreCase)) as SlideShowField;
        }
    }
}