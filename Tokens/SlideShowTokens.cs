﻿using System.Collections.Generic;
using System.Linq;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Models;
using Orchard.ContentManagement;
using Orchard.Localization;
using Orchard.Tokens;

namespace Onestop.SlideShows.Tokens {
    public class SlideShowTokens : ITokenProvider {
        private readonly IContentManager _contentManager;

        public SlideShowTokens(IContentManager contentManager) {
            _contentManager = contentManager;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        public void Describe(DescribeContext context) {
            context.For("Content", T("Content Items"), T("Content Items"))
                   .Token("Slides", T("Slides"), T("The slides for this slide show content item."))
                   .Token("Slides[n]", T("nth slide"), T("The slide at the index provided."));
            context.For("Slides", T("Slide collections"), T("Slide collections"))
                   .Token("Count", T("Count"), T("The number of slides in the slide show."))
                   .Token("First", T("First"), T("The first slide in the slide show."))
                   .Token("Last", T("Last"), T("The last slide in the slide show."));
        }

        public void Evaluate(EvaluateContext context) {
            context.For<IContent>("Content")
                   .Token("Slides", i => i == null ? "" : _contentManager.GetItemMetadata(i).DisplayText)
                   .Chain("Slides", "Slides", GetSlides);
            if (context.Target == "Content") {
                var forContent = context.For<IContent>("Content");
                if (forContent != null && forContent.Data != null && forContent.Data.ContentItem != null) {
                    var slideshow = forContent.Data.ContentItem.As<SlideShowPart>();
                    if (slideshow != null) {
                        var slides = slideshow.Slides.ToList();
                        for (var index = 0; index < slides.Count; index++) {
                            var tokenName = "Slides[" + index + "]";
                            var closureIndex = index;
                            forContent.Token(tokenName,
                                             c => _contentManager.GetItemMetadata(
                                                 c.As<SlideShowPart>().Slides.ElementAt(closureIndex)).DisplayText);
                            forContent.Chain(tokenName, "TemplatedItem",
                                             c => c.As<SlideShowPart>().Slides.ElementAt(closureIndex).As<TemplatedItemPart>());
                        }
                    }
                }
            }

            context.For<IEnumerable<SlidePart>>("Slides")
                   .Token("Count", v => v.Count())
                   .Token("First", v => {
                       var first = v.FirstOrDefault();
                       return first == null
                                  ? ""
                                  : _contentManager.GetItemMetadata(first).DisplayText;
                   })
                   .Chain("First", "TemplatedItem", v => v.FirstOrDefault().As<TemplatedItemPart>())
                   .Token("Last", v => {
                       var last = v.LastOrDefault();
                       return last == null
                                  ? ""
                                  : _contentManager.GetItemMetadata(last).DisplayText;
                   })
                   .Chain("Last", "TemplatedItem", v => v.LastOrDefault().As<TemplatedItemPart>());
        }

        private IEnumerable<SlidePart> GetSlides(IContent content) {
            if (!content.Has<SlideShowPart>()) return new SlidePart[0];
            return content.As<SlideShowPart>().Slides;
        }
    }
}