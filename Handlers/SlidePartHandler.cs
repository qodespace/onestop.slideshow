﻿using System.Linq;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Core.Title.Models;

namespace Onestop.SlideShows.Handlers {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlidePartHandler : ContentHandler {
        private readonly ISlideShowService _slideShowService;
        private readonly IContentManager _contentManager;

        public SlidePartHandler(ISlideShowService slideShowService, IContentManager contentManager) {
            _slideShowService = slideShowService;
            _contentManager = contentManager;
            OnRemoved<SlidePart>(UpdateSlideShowSlides);
        }

        private void UpdateSlideShowSlides(RemoveContentContext context, SlidePart part) {
            var container = _contentManager.Get(part.ContainerId, VersionOptions.DraftRequired);
            var slideShow = _slideShowService.GetSlideShow(container, null);
            var slides = slideShow.SlideIds.ToList();

            slides.Remove(part.Id);
            slideShow.SlideIds = slides;
        }

        protected override void GetItemMetadata(GetContentItemMetadataContext context)
        {
           var slide = context.ContentItem.As<SlidePart>();
           if (slide != null)
           {
               context.Metadata.DisplayText = context.ContentItem.ContentType + "-" + context.ContentItem.Id.ToString();
           }
        }
    }
}