﻿using System.Linq;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;

namespace Onestop.SlideShows.Handlers {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlideShowPartHandler : ContentHandler {
        private readonly ISlideShowService _slideShowService;

        public SlideShowPartHandler(ISlideShowService slideShowService) {
            _slideShowService = slideShowService;
            OnActivated<SlideShowPart>(SetupFields);
        }

        private void SetupFields(ActivatedContentContext context, SlideShowPart part) {
            part.LazySlides.Loader(() => _slideShowService.GetSlidesFor(part).ToList());
        }
    }
}