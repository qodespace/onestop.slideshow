﻿using System.Linq;
using Onestop.SlideShows.Fields;
using Onestop.SlideShows.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Handlers;
using Orchard.ContentManagement.MetaData;
using Orchard.ContentManagement.Utilities;
using Orchard.Data;

namespace Onestop.SlideShows.Handlers {
    public class SlideShowFieldHandler : ContentHandler {
        private readonly IContentManager _contentManager;
        private readonly IContentDefinitionManager _contentDefinitionManager;

        public SlideShowFieldHandler(
            IContentManager contentManager, 
            IContentDefinitionManager contentDefinitionManager,
            IRepository<SlideShowPartRecord> repository) {
            
            _contentManager = contentManager;
            _contentDefinitionManager = contentDefinitionManager;

            Filters.Add(StorageFilter.For(repository));
        }

        protected override void Loading(LoadContentContext context) {
            base.Loading(context);

            var fields = context.ContentItem.Parts.SelectMany(x => x.Fields.Where(f => f.FieldDefinition.Name == typeof (SlideShowField).Name)).Cast<SlideShowField>();
            
            // define lazy initializer for SlideShowField.Slides
            var contentTypeDefinition = _contentDefinitionManager
                .GetTypeDefinition(context.ContentType);
            if (contentTypeDefinition == null) {
                return;
            }

            foreach (var field in fields) {
                var localField = field;
                var mySlides = _contentManager.GetMany<SlidePart>(localField.SlideIds, VersionOptions.Published, QueryHints.Empty).ToList();
                //TODO 1.9: verify that this works
                field.LazySlides.Loader(() => _contentManager.GetMany<SlidePart>(localField.SlideIds, VersionOptions.Published, QueryHints.Empty));
            
            }
        }
    }
}