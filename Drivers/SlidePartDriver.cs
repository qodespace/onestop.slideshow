﻿using Onestop.SlideShows.Models;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;
using Orchard.Localization;

namespace Onestop.SlideShows.Drivers {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlidePartDriver : ContentPartDriver<SlidePart> {
        private readonly IContentManager _contentManager;

        public SlidePartDriver(IContentManager contentManager) {
            _contentManager = contentManager;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        protected override string Prefix {
            get { return "OnestopSlide"; }
        }

        protected override DriverResult Display(SlidePart part, string displayType, dynamic shapeHelper) {
            return null;
        }

        protected override DriverResult Editor(SlidePart part, dynamic shapeHelper) {
            return null;
        }

        protected override DriverResult Editor(SlidePart part, IUpdateModel updater, dynamic shapeHelper) {
            return Editor(part, shapeHelper);
        }

        protected override void Importing(SlidePart part, ImportContentContext context) {
            var slideElement = context.Data.Element("SlidePart");
            if (slideElement != null) {
                var slideshowAttribute = slideElement.Attribute("slideshow");
                if (slideshowAttribute != null) {
                    var slideshow = context.GetItemFromSession(slideshowAttribute.Value);
                    if (slideshow != null) {
                        part.ContainerId = slideshow.Id;
                    }
                }
            }
        }

        protected override void Exporting(SlidePart part, ExportContentContext context) {
            var elt = context.Element("SlidePart");
            var container = _contentManager.Get(part.ContainerId);
            elt.SetAttributeValue("container", _contentManager.GetItemMetadata(container).Identity);
        }
    }
}