﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onestop.Layouts.Drivers;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Onestop.SlideShows.Settings;
using Onestop.SlideShows.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;
using Orchard.Environment.Extensions;

namespace Onestop.SlideShows.Drivers {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlideShowPartDriver : ContentPartDriver<SlideShowPart> {
        private readonly ISlideShowService _slideShowService;
        private readonly ITemplatedItemPartDriver _templatedItemPartDriver;
        private readonly IWorkContextAccessor _wca;

        public SlideShowPartDriver(
            ISlideShowService slideShowService,
            ITemplatedItemPartDriver templatedItemPartDriver,
            IWorkContextAccessor wca) {

            _slideShowService = slideShowService;
            _templatedItemPartDriver = templatedItemPartDriver;
            _wca = wca;
        }

        protected override string Prefix {
            get { return "OnestopSlideShow"; }
        }

        protected override DriverResult Display(SlideShowPart part, string displayType, dynamic shapeHelper) {
            return Combined(
                ContentShape("Parts_SlideShow", () => {
                    var settings = part.TypePartDefinition.Settings.GetModel<SlideShowSettings>();
                    ApplySettings(part, settings);

                    var slides = _slideShowService.GetSlides(part.SlideIds, VersionOptions.Published).Where(x => x.ContentItem.VersionRecord.Latest == true && x.ContentItem.VersionRecord.Published == true).ToList();
                    var slideShapes = slides.Select((s, i) => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), displayType, shapeHelper).SlideIndex(i));
                    var layouts = _slideShowService.GetLayoutsFor(slides).ToList();
                    var any = layouts.Any();
                    var width = any ? layouts.Max(l => l.Width) : 0;
                    var height = any ? layouts.Max(l => l.Height) : 0;
                    var pluginShape = _slideShowService.GetPluginDisplayShape(part);
                    var layout = _wca.GetContext().Layout;
                    var indexObject = layout.OnestopSlideShowIndex;
                    int slideShowIndex = indexObject == null ? 0 : indexObject;

                    layout.OnestopSlideShowIndex = slideShowIndex + 1;

                    pluginShape
                        .Id("slide-show-" + slideShowIndex)
                        .Width(width)
                        .Height(height)
                        .SlideShow(part)
                        .Slides(slideShapes);

                    return shapeHelper.Parts_SlideShow(
                        Plugin: pluginShape);
                }),
                ContentShape("Parts_SlideShow_SummaryAdmin", () => {
                    var slides = _slideShowService.GetSlides(part.SlideIds, VersionOptions.AllVersions).ToList();
                    var slideShapes = slides.Select((s, i) => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), displayType, shapeHelper).SlideIndex(i));
                    var layouts = _slideShowService.GetLayoutsFor(slides).ToList();
                    var any = layouts.Any();
                    var width = any ? layouts.Max(l => l.Width) : 0;
                    var height = any ? layouts.Max(l => l.Height) : 0;
                    var layout = _wca.GetContext().Layout;
                    var indexObject = layout.OnestopSlideShowIndex;
                    int slideShowIndex = indexObject == null ? 0 : indexObject;

                    return shapeHelper.Parts_SlideShow_SummaryAdmin(
                        ContentItem: part.ContentItem,
                        Id: "slide-show-" + slideShowIndex,
                        Width: width,
                        Height: height,
                        SlideShow: part,
                        Slides: slideShapes);
                }));
        }

        protected override DriverResult Editor(SlideShowPart part, dynamic shapeHelper) {
            return ContentShape(
                "Parts_SlideShow_Edit",
                () => {
                    var settings = part.TypePartDefinition.Settings.GetModel<SlideShowSettings>();
                    ApplySettings(part, settings);
                    var slides = _slideShowService.GetSlidesFor(part, VersionOptions.Latest).ToList();
                    var pluginEditorShapes = _slideShowService.GetConfigurationEditorsFor(part);
                    var plugin = part.Plugin;
                    if (string.IsNullOrWhiteSpace(plugin)) {
                        var plugins = _slideShowService.GetPlugins().ToList();
                        if (plugins.Any()) {
                            plugin = plugins.First().Name;
                        }
                    }
                    var defaultSlideType = String.IsNullOrWhiteSpace(part.DefaultSlideType)
                        ? String.IsNullOrWhiteSpace(settings.DefaultSlideType)
                            ? "Slide"
                            : settings.DefaultSlideType
                        : part.DefaultSlideType;

                    return shapeHelper.EditorTemplate(
                        TemplateName: "Parts/SlideShow",
                        Model: new SlideShowViewModel {
                            SlideShow = part,
                            SlideShapes = slides.Select(s => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), "SummaryAdmin", shapeHelper)),
                            SlideOrder = String.Join(",", slides.Select(s => s.Id)),
                            Plugin = plugin,
                            PluginConfigurationEditors = pluginEditorShapes,
                            AllowPluginOverride = settings.AllowPluginOverride,
                            AllowPluginConfigurationOverride = settings.AllowPluginConfigurationOverride,
                            CanAddSlide = !settings.PreventTemplateChoice,
                            AllowSlideTypeConfiguration = settings.AllowSlideTypeChoice,
                            AllowSlideTypeChoice = part.AllowSlideTypeChoice,
                            DefaultSlideType = defaultSlideType,
                            SlideContentTypes = _slideShowService.GetSlideTypes().ToDictionary(t => t.Name, t => t.DisplayName)
                        },
                        Prefix: Prefix);
                });
        }

        protected override DriverResult Editor(SlideShowPart part, IUpdateModel updater, dynamic shapeHelper) {
            var model = new SlideShowViewModel();
            if (updater.TryUpdateModel(model, Prefix, null, null)) {
                if (model.SlideOrder != null) {
                    var slideIds = model.SlideOrder.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                    part.SlideIds = slideIds;
                }
                part.AllowSlideTypeChoice = model.AllowSlideTypeChoice;
                part.DefaultSlideType = model.DefaultSlideType;
            }

            _slideShowService.UpdatePluginConfiguration(part, Prefix, updater);

            return Editor(part, shapeHelper);
        }

        protected override void Importing(SlideShowPart part, ImportContentContext context) {
            context.ImportAttribute(part.PartDefinition.Name, "SlideIds", s => s.Split(',')
                .Select(context.GetItemFromSession)
                .Where(x => x != null)
                .Select(x => x.Id));

            var plugin = context.Data.Element(part.PartDefinition.Name).Element("Plugin");
            var pluginConfiguration = context.Data.Element(part.PartDefinition.Name).Element("PluginConfiguration");
            
            part.Plugin = plugin != null ? plugin.Value : null;
            part.PluginConfiguration = pluginConfiguration != null ? pluginConfiguration.Value : null;
        }

        protected override void Exporting(SlideShowPart part, ExportContentContext context) {
            var slides = _slideShowService.GetSlides(part.SlideIds).ToList();
            var itemIds = slides.Select(x => context.ContentManager.GetItemMetadata(x).Identity.ToString());
            context.Element(part.PartDefinition.Name).SetAttributeValue("SlideIds", String.Join(",", itemIds));
            context.Element(part.PartDefinition.Name).SetElementValue("Plugin", part.Plugin);
            context.Element(part.PartDefinition.Name).SetElementValue("PluginConfiguration", part.PluginConfiguration);
        }

        private static void ApplySettings(SlideShowPart part, SlideShowSettings settings) {
            if (!settings.AllowPluginOverride || string.IsNullOrWhiteSpace(part.Plugin)) {
                part.Plugin = settings.Plugin;
            }
            var defaultConfig = part.TypePartDefinition.Settings.GetModel<Dictionary<string, string>>("SlideShowSettings");
            if (settings.AllowPluginConfigurationOverride) {
                part.SetFallbackConfiguration(defaultConfig);
            }
            else {
                part.SetConfiguration(defaultConfig);
            }
        }
    }
}