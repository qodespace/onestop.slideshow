﻿using System;
using System.Collections.Generic;
using System.Linq;
using Onestop.Layouts.Drivers;
using Onestop.Layouts.Helpers;
using Onestop.Layouts.Models;
using Onestop.SlideShows.Fields;
using Onestop.SlideShows.Models;
using Onestop.SlideShows.Services;
using Onestop.SlideShows.Settings;
using Onestop.SlideShows.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.Drivers;
using Orchard.ContentManagement.Handlers;

using Orchard.Localization;

namespace Onestop.SlideShows.Drivers {
    public class SlideShowFieldDriver : ContentFieldDriver<SlideShowField> {
        private readonly IContentManager _contentManager;
        private readonly ISlideShowService _slideShowService;
        private readonly ITemplatedItemPartDriver _templatedItemPartDriver;
        private readonly IWorkContextAccessor _wca;

        public SlideShowFieldDriver(
            IContentManager contentManager,
            ISlideShowService slideShowService,
            ITemplatedItemPartDriver templatedItemPartDriver,
            IWorkContextAccessor wca) {

            _contentManager = contentManager;
            _slideShowService = slideShowService;
            _templatedItemPartDriver = templatedItemPartDriver;
            _wca = wca;
            T = NullLocalizer.Instance;
        }

        public Localizer T { get; set; }

        private static string GetPrefix(SlideShowField field, ContentPart part) {
            return part.PartDefinition.Name + "." + field.Name;
        }

        private static string GetDifferentiator(SlideShowField field) {
            return field.Name;
        }

        protected override DriverResult Display(ContentPart part, SlideShowField field, string displayType, dynamic shapeHelper) {
            return Combined(
                ContentShape("Fields_SlideShow", GetDifferentiator(field), () => {
                    var settings = field.PartFieldDefinition.Settings.GetModel<SlideShowSettings>();
                    ApplySettings(field, settings);
                    var slides = _slideShowService.GetSlides(field.SlideIds, VersionOptions.Published).Distinct(new SlideComparer()).Where(x => x.ContentItem.VersionRecord.Latest == true && x.ContentItem.VersionRecord.Published == true).ToList();
                    var slideShapes = slides.Select((s, i) => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), displayType, shapeHelper).SlideIndex(i));
                    var layout = _wca.GetContext().Layout;
                    var indexObject = layout.OnestopSlideShowIndex;
                    int slideShowIndex = indexObject == null ? 0 : indexObject;
                    var layouts = _slideShowService.GetLayoutsFor(slides).ToList();
                    var width = layouts.Any() ? layouts.Max(l => l.Width) : 0;
                    var height = layouts.Any() ? layouts.Max(l => l.Height) : 0;
                    var pluginShape = _slideShowService.GetPluginDisplayShape(field);

                    layout.OnestopSlideShowIndex = slideShowIndex + 1;

                    pluginShape
                        .Id("slide-show-" + slideShowIndex)
                        .Width(width)
                        .Height(height)
                        .SlideShow(part)
                        .Slides(slideShapes);

                    foreach(var alternate in new List<string>(pluginShape.Metadata.Alternates))
                        pluginShape.Metadata.Alternates.Add(alternate + "__" + field.PartFieldDefinition.Name);
                    pluginShape.Metadata.Alternates.Add(pluginShape.Metadata.Type + "__" + field.PartFieldDefinition.Name);
                        
                    return shapeHelper.Fields_SlideShow(Plugin: pluginShape);
                }),
                ContentShape("Fields_SlideShow_SummaryAdmin", () => {
                    var slides = _slideShowService.GetSlides(field.SlideIds, VersionOptions.Published).ToList();
                    var slideShapes = slides.Select((s, i) => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), displayType, shapeHelper).SlideIndex(i));
                    var layouts = _slideShowService.GetLayoutsFor(slides).ToList();
                    var any = layouts.Any();
                    var width = any ? layouts.Max(l => l.Width) : 0;
                    var height = any ? layouts.Max(l => l.Height) : 0;
                    var layout = _wca.GetContext().Layout;
                    var indexObject = layout.OnestopSlideShowIndex;
                    int slideShowIndex = indexObject == null ? 0 : indexObject;

                    return shapeHelper.Fields_SlideShow_SummaryAdmin(
                        ContentItem: part.ContentItem,
                        Id: "slide-show-" + slideShowIndex,
                        Width: width,
                        Height: height,
                        SlideShow: part,
                        Slides: slideShapes);
                }));
        }

        protected override DriverResult Editor(ContentPart part, SlideShowField field, dynamic shapeHelper) {
            return ContentShape("Fields_SlideShow_Edit", GetDifferentiator(field), () => {
                    var settings = field.PartFieldDefinition.Settings.GetModel<SlideShowSettings>();
                    ApplySettings(field, settings);
                    var slides = _slideShowService.GetSlides(field.SlideIds).ToList();
                    var slideShapes = slides.Select( s => _templatedItemPartDriver.GetDisplayShape(s.As<TemplatedItemPart>(), "SummaryAdmin", shapeHelper));
                    var layouts = _slideShowService.GetLayoutsFor(slides);
                    var pluginEditorShapes = _slideShowService.GetConfigurationEditorsFor(field);
                    var plugin = field.Plugin;
                    if (String.IsNullOrWhiteSpace(plugin)) {
                        var plugins = _slideShowService.GetPlugins().ToList();
                        if (plugins.Any()) {
                            plugin = plugins.First().Name;
                        }
                    }
                    var defaultSlideType = String.IsNullOrWhiteSpace(field.DefaultSlideType)
                        ? String.IsNullOrWhiteSpace(settings.DefaultSlideType)
                            ? "Slide"
                            : settings.DefaultSlideType
                        : field.DefaultSlideType;

                    var model = new SlideShowViewModel {
                        ContentPart = part,
                        Field = field,
                        Slides = slides,
                        SlideIds = String.Join(",", slides.Select(s => s.Id)),
                        SlideShapes = slideShapes,
                        SlideOrder = String.Join(",", slides.Select(s => s.Id)),
                        Layouts = layouts,
                        Plugin = plugin,
                        PluginConfigurationEditors = pluginEditorShapes,
                        AllowPluginOverride = settings.AllowPluginOverride,
                        AllowPluginConfigurationOverride = settings.AllowPluginConfigurationOverride,
                        CanAddSlide = !settings.PreventTemplateChoice,
                        AllowSlideTypeConfiguration = settings.AllowSlideTypeChoice,
                        AllowSlideTypeChoice = field.AllowSlideTypeChoice,
                        DefaultSlideType = defaultSlideType,
                        SlideContentTypes = _slideShowService.GetSlideTypes().ToDictionary(t => t.Name, t => t.DisplayName)
                    };

                    return shapeHelper.EditorTemplate(TemplateName: "Fields/SlideShow", Model: model, Prefix: GetPrefix(field, part));
                });
        }

        protected override DriverResult Editor(ContentPart part, SlideShowField field, IUpdateModel updater, dynamic shapeHelper) {
            var model = new SlideShowViewModel();
            var prefix = GetPrefix(field, part);

            updater.TryUpdateModel(model, prefix, null, null);

            field.AllowSlideTypeChoice = model.AllowSlideTypeChoice;
            field.DefaultSlideType = model.DefaultSlideType;

            var slideIds = String.IsNullOrEmpty(model.SlideIds) ?
                new int[0] :
                model.SlideIds.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            var slides = _contentManager.GetMany<SlidePart>(slideIds, VersionOptions.Published, QueryHints.Empty);
            slides.Each((s, i) => _contentManager.Publish(s.ContentItem));

            field.SlideIds = slideIds;
            _slideShowService.UpdatePluginConfiguration(field, prefix, updater);

            return Editor(part, field, shapeHelper);
        }

        protected override void Importing(ContentPart part, SlideShowField field, ImportContentContext context) {
            var contentItemIds = context.Attribute(field.FieldDefinition.Name + "." + field.Name, "ContentItems");
            if (contentItemIds != null) {
                field.SlideIds = contentItemIds.Split(',')
                    .Select(context.GetItemFromSession)
                    .Select(contentItem => contentItem.Id).ToArray();
            }
            else {
                field.SlideIds = new int[0];
            }
        }

        protected override void Exporting(ContentPart part, SlideShowField field, ExportContentContext context) {
            if (field.SlideIds.Any()) {
                var contentItemIds = field.SlideIds
                    .Select(x => _contentManager.Get(x))
                    .Select(x => _contentManager.GetItemMetadata(x).Identity.ToString())
                    .ToArray();

                context.Element(field.FieldDefinition.Name + "." + field.Name).SetAttributeValue("ContentItems", string.Join(",", contentItemIds));
            }
        }

        protected override void Describe(DescribeMembersContext context) {
            context
                .Member(null, typeof(string), T("Ids"), T("A formatted list of the slide ids, e.g. 1,42"));
        }

        private static void ApplySettings(SlideShowField field, SlideShowSettings settings) {
            if (!settings.AllowPluginOverride || string.IsNullOrWhiteSpace(field.Plugin)) {
                field.Plugin = settings.Plugin;
            }
            var defaultConfig =
                field.PartFieldDefinition.Settings
                     .GetModel<Dictionary<string, string>>("SlideShowSettings");
            if (settings.AllowPluginConfigurationOverride) {
                field.SetFallbackConfiguration(defaultConfig);
            }
            else {
                field.SetConfiguration(defaultConfig);
            }
        }
    }
}