﻿using System.Collections.Generic;
using Orchard.Environment.Extensions;
using Orchard.Environment.Extensions.Models;
using Orchard.Security.Permissions;

namespace Onestop.SlideShows {
    [OrchardFeature("Onestop.SlideShows")]
    public class Permissions : IPermissionProvider {
        public static readonly Permission ManageSlideShows = new Permission { Description = "Manage Slide Shows", Name = "ManageSlideShows" };

        public virtual Feature Feature { get; set; }

        public IEnumerable<Permission> GetPermissions() {
            return new[] {
                ManageSlideShows
            };
        }

        public IEnumerable<PermissionStereotype> GetDefaultStereotypes() {
            return new[] {
                new PermissionStereotype {
                    Name = "Administrator",
                    Permissions = new[] { ManageSlideShows }
                },
                new PermissionStereotype {
                    Name = "Author",
                    Permissions = new[] { ManageSlideShows }
                }
            };
        }
    }
}