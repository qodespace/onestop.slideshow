﻿using Onestop.Layouts.Models;
using Orchard.ContentManagement;

namespace Onestop.SlideShows.Models {
    public class SlidePart : ContentPart, ISecondaryContent {

        public int ContainerId {
            get { return this.Retrieve(x => x.ContainerId, versioned: true); }
            set { this.Store(x => x.ContainerId, value, versioned: true); }
        }
        
        public IContent GetPrimaryContentItem() {
            var slideShow = ContentItem.ContentManager.Get(ContainerId);
            return (IContent)slideShow ?? this;
        }
    }
}