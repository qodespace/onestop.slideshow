﻿using Orchard.ContentManagement.Records;

namespace Onestop.SlideShows.Models {
    // We need this in order to be able to efficiently query all slide shows
    public class SlideShowPartRecord : ContentPartRecord {
    }
}
