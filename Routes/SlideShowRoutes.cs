﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Environment.Extensions;
using Orchard.Mvc.Routes;

namespace Onestop.SlideShows.Routes {
    [OrchardFeature("Onestop.SlideShows")]
    public class SlideShowRoutes : IRouteProvider {
        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes()) {
                routes.Add(routeDescriptor);
            }
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Route = new Route(
                        "Admin/Onestop/SlideShow/{action}",
                        new RouteValueDictionary {
                            {"area", "Onestop.SlideShows"},
                            {"controller", "SlideShowAdmin"},
                            {"action", "List"}
                        },
                        new RouteValueDictionary(),
                        new RouteValueDictionary {
                            {"area", "Onestop.SlideShows"}
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}
